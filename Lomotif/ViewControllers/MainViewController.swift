//
//  MainViewController.swift
//  Lomotif
//
//  Created by Dilum Navanjana on 7/15/15.
//  Copyright (c) 2015 Dilum Navanjana. All rights reserved.
//

import UIKit

let url = "https://itunes.apple.com/sg/rss/topsongs/limit=50/explicit=true/json" //URL to load once the viewcontroller loads

class MainViewController: UIViewController, UITableViewDataSource,UITableViewDelegate,NetworkDelegate{

    @IBOutlet var tableView: UITableView! //TableView
    var arrData :NSMutableArray = NSMutableArray() //Data MutableArray
    var networkController :NetworkController = NetworkController() //NetworkController to use in all calls
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialLoad() //Load initial data for the TableView
    }
    
    func initialLoad () {
        //Start Loading with MBProgressHUD
        let loadingNotification = MBProgressHUD.showHUDAddedTo(self.view, animated: true)
        loadingNotification.mode = MBProgressHUDMode.Indeterminate
        loadingNotification.labelText = "Loading" //Loading Text
        
        networkController.delegate = self //Setting the delegate to self
        networkController.getJsonData(url) //Get data with url Constant
    }
    
    // MARK: - NetworkDelegate
    
    func responseWithSuccess(result: NSDictionary) { //NetworkDelegate with the response
        
        //Stop Loading
        MBProgressHUD.hideAllHUDsForView(self.view, animated: true)
        
        var feed: NSDictionary = result.valueForKey("feed") as! NSDictionary
        var entry: NSArray  = feed.valueForKey("entry") as! NSArray
        setModelWithResponse(entry)
    }
    
    // MARK: - Custom Implementation
    
    func setModelWithResponse (arrEntry: NSArray) //Add data to the Model array : arrData
    {
        for (element) in  arrEntry
        {
            //Create a new PublicModel & add it to arrData MutableArray
            var album :Album = Album.new()
            var model =  album.initWithData(element as! NSDictionary)
            arrData.addObject(model)
        }
        tableView.reloadData() //Finally reload the tableView
    }
    
    // MARK: - UITableView Datasource
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrData.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let identifier = "CELL"
        var cell: AlbumCell! = tableView.dequeueReusableCellWithIdentifier(identifier) as? AlbumCell //Album with xib
        if cell == nil {
            tableView.registerNib(UINib(nibName: "AlbumCell", bundle: nil), forCellReuseIdentifier: identifier)
            cell = tableView.dequeueReusableCellWithIdentifier(identifier) as? AlbumCell
        }
        
        cell.imgView.image = nil //Remove if there is a existing image
        
        //
        //Set Cell Values from arrData here
        //
        
        var album : Album = arrData.objectAtIndex(indexPath.row) as! Album //Get the Album for the cell from indexPath
        
        cell.lblTitle.text = album.strTitle as? String //Set the Title UILabel
        cell.lblDescription.text = album.strDescription as? String //Set the Description UILabel
        
        if let image_url = NSURL(string: ((album.strImageURL)! as? String)!) //NSURL from the strImageURL
        {
            cell.imgView.setImageWithUrl(image_url, placeHolderImage: nil) //Using UIImageView+AFNetworking class to do the asynchronize image loading inside the cell
        }
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }

}
