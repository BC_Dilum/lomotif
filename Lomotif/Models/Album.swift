//
//  Album.swift
//  Lomotif
//
//  Created by Dilum Navanjana on 7/15/15.
//  Copyright (c) 2015 Dilum Navanjana. All rights reserved.
//

import UIKit

class Album: NSObject {
   
    var strTitle :NSString! //Album Title
    var strDescription :NSString! //Album Description
    var strImageURL :NSString! //Album Image URL
    
    func initWithData (data:NSDictionary) -> Album //Return self with Album object
    {
        //Set relevent values from the json here
        strTitle = data.valueForKey("im:name")?.valueForKey("label") as! NSString
        strDescription = data.valueForKey("im:artist")?.valueForKey("label") as! NSString
        strImageURL = data.valueForKey("im:image")?.objectAtIndex(2).valueForKey("label") as! NSString
        
        return self
    }
    
}
