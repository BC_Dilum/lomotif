//
//  NetworkController.swift
//  Lomotif
//
//  Created by Dilum Navanjana on 7/15/15.
//  Copyright (c) 2015 Dilum Navanjana. All rights reserved.
//

import UIKit

//This is the Network Controller which use to call all Async network
//Get Post calls
//For now there is only one network call. That is getJsonData

//This is the NetworkDelegate that will give the response of the network response as a NSDictionary
protocol NetworkDelegate {
    func responseWithSuccess(result: NSDictionary) //Add this method to your ViewController to take the response back
}

class NetworkController: NSObject {
   
    var delegate: NetworkDelegate?
    
    //Get network call with a URL
    func getJsonData (url:NSString)
    {
        //URLRequest for get Json Data
        let request = NSMutableURLRequest(URL: NSURL(string:url as String)!)
        request.HTTPMethod = "GET" //GET method
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { //Block for the response
            data, response, error in
            
            if error != nil { //Error request
                println("error=\(error)")
                return
            }
            
            NSOperationQueue.mainQueue().addOperationWithBlock { //Back to Main thread
                var err: NSError?
                if let jsonResult = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &err) as? NSDictionary //Check the response is a NSDictionary
                {
                   self.delegate?.responseWithSuccess(jsonResult) //Delegate method take the data back to the viewcontroller from here
                }
            }
        }
        task.resume()
    }
}
