//
//  AlbumCell.swift
//  Lomotif
//
//  Created by Dilum Navanjana on 7/15/15.
//  Copyright (c) 2015 Dilum Navanjana. All rights reserved.
//

import UIKit

//This is the Custom cell with xib to create custom styles for the TableView

class AlbumCell: UITableViewCell {
    
    //List of Properties inside the cell
    @IBOutlet var imgView: UIImageView! //ImageView to show the Album Art
    @IBOutlet var lblTitle: UILabel! //Title of the Album
    @IBOutlet var lblDescription: UILabel! //Description of the Album
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
